package java8;

import entity.Employee;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Java8Coading {
    public static void main(String[] args) {
        //find duplicate characters in string
        String s = "asdwerasdzxaaswesdsdsadf";
        Map<String, Long> duplicateChar = Arrays.stream(s.split("")).collect(Collectors.groupingBy(
                Function.identity(), Collectors.counting()));
        System.out.println("character :"+duplicateChar);

        //find duplicate words in string
        String s1 = "sai kiran codes java java is a prog language";
        Map<String, Long> duplicatewords = Arrays.stream(s1.split(" ")).collect(Collectors.groupingBy(
                Function.identity(), Collectors.counting()));
        System.out.println("Words :"+duplicatewords);


        //find heighest paid employee by department
        List<Employee> employees = Employee.getEmployees();

        System.out.println(employees);
        Map<String, Optional<Employee>> collect = employees.stream().collect(Collectors.groupingBy(Employee::getDeptId, Collectors.maxBy(Comparator.comparing(Employee::getSalary))));
        System.out.println(" employee : " +collect);




    }



}
