package entity;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Employee {

    private int empId;
    private String name;
    private double salary;
    private String deptId;
    private String location;

    public Employee(int empId, String name, double salary, String deptId, String location) {
        this.empId = empId;
        this.name = name;
        this.salary = salary;
        this.deptId = deptId;
        this.location = location;
    }

    public Employee() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return getEmpId() == employee.getEmpId() && Double.compare(employee.getSalary(), getSalary()) == 0 && Objects.equals(getName(), employee.getName()) && Objects.equals(getDeptId(), employee.getDeptId()) && Objects.equals(getLocation(), employee.getLocation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmpId(), getName(), getSalary(), getDeptId(), getLocation());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empId=" + empId +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", deptId='" + deptId + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static List<Employee> getEmployees(){
        List<Employee> employees = new ArrayList<>();
        Employee e1 = new Employee();
        e1.setDeptId("DEV"); e1.setEmpId(1); e1.setName("ABC"); e1.setSalary(1000); e1.setLocation("Bangalore");
        employees.add(e1);
        Employee e2 = new Employee();
        e2.setDeptId("DEV"); e2.setEmpId(2); e2.setName("ABC1"); e2.setSalary(2000); e2.setLocation("Bangalore");
        employees.add(e2);
        Employee e3 = new Employee();
        e3.setDeptId("QA"); e3.setEmpId(3); e3.setName("ABC2"); e3.setSalary(3000); e3.setLocation("hyd");
        employees.add(e3);
        Employee e4 = new Employee();
        e4.setDeptId("QA"); e4.setEmpId(4); e4.setName("ABC3"); e4.setSalary(4000); e4.setLocation("hyd");
        employees.add(e4);
        Employee e5 = new Employee();
        e5.setDeptId("UI"); e5.setEmpId(5); e5.setName("ABC4"); e5.setSalary(5000); e5.setLocation("hyd");
        employees.add(e5);
        Employee e6 = new Employee();
        e6.setDeptId("UI"); e6.setEmpId(6); e6.setName("ABC5"); e6.setSalary(6000); e6.setLocation("che");
        employees.add(e6);

        return employees;
    }
}
